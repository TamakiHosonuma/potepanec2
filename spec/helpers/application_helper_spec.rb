require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    context "ページタイトルが存在する場合" do
      it "ページタイトル-BIGBAG Storeと表示" do
        expect(helper.full_title('hoge')).to eq 'hoge - BIGBAG Store'
      end
    end

    context "ページタイトルがnilの場合" do
      it "BIGBAG Storeのみ表示" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end

    context "ページタイトルが空白の場合" do
      it "BIGBAG Storeのみ表示" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end
  end
end
