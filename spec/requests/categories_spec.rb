require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon_a) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_a) { create(:product, taxons: [taxon_a]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '正常なレスポンスを返すこと' do
      expect(response).to have_http_status "200"
    end

    it 'taxonomyが表示されること' do
      expect(response.body).to include taxonomy.name
    end

    it 'taxonが表示されること' do
      expect(response.body).to include taxon.name
      expect(response.body).to include taxon_a.name
    end

    it '商品数が表示されること' do
      expect(response.body).to include taxon.products.count.to_s
    end

    it '商品が表示されること' do
      expect(response.body).to include product.name
    end

    it '選択したtaxonに紐付かない商品は表示されないこと' do
      expect(response.body).not_to include product_a.name
    end

    it '商品価格が表示されること' do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
