RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it '正常なレスポンスを返すこと' do
      expect(response).to have_http_status "200"
    end

    it '商品概要が表示されること' do
      expect(response.body).to include product.name
      expect(response.body).to include product.description
      expect(response.body).to include product.display_price.to_s
    end
  end
end
