require 'rails_helper'

RSpec.describe "Categories", type: :system, js: true do
  describe '正しく表示されること' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxons) { taxonomy.root.children.create(name: "Categories") }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxons.id) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before { visit potepan_category_path(taxon.id) }

    it 'カテゴリー名をクリックした際、クリックしたカテゴリーページへ移動すること' do
      click_on taxonomy.name
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '商品名をクリックした際、クリックした商品の詳細ページへ移動すること' do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
